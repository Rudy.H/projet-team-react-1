import axios from 'axios';

export default axios.create( {
    baseURL: 'http://159.65.28.230:3080/auth/',
    headers: {
        "Content-type": "application/json"
    }
});
