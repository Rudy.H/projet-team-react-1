import React from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Login from "./Components/Authentificator/authentificator"
import AddLesson from "./Components/Main-page/lesson-name";
import AccessLesson from './Components/Main-page/access_lesson'
import Formations from "./Components/Front-client/Formations";

function App() {

    if (!localStorage.user) {
        return (<Login/>
        )
    }

    return (
        <>
            <Router>
                <Route path="/formation" component={Formations}/>
                <Route exact path="/" component={Login}/>
                <Route exact path="/admin" component={AccessLesson}/>
                <Route path="/admin/:id" children={<AddLesson/>}/>
            </Router>
        </>
    );
}

export default App;
