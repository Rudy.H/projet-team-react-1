import React from "react"
import 'bootstrap/dist/css/bootstrap.min.css';


function Entete(props){
    console.log(props);
    return(
        <div style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            height: "90px",
            borderBottom: "solid 1px #DFDFDF",
            borderLeft: "solid 6px black"
        }}>
            <h1 style={{
                margin:"0 0 0 55px",
                fontSize: "26px",
                fontWeight: "700"
            }}>Nom de la formation {props.formationName}</h1>
        </div>
    )
}

export default Entete