import React, {useState} from 'react'

function PlayVideo(props) {
    console.log(props);

    const [lesson, setLesson] = useState('');

    function GetYoutubeURL(URL) {
        return URL.split('=')[1];

    }

    return (
        <div style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            height: "600px",
            marginTop: "10px"
        }}>
            <div style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: "70px",
                borderTop: "solid 5px black",
                width: "13%"
            }}>

                <h3>Video</h3>

            </div>
            <iframe id="ytplayer" type="text/html" width="640" height="360"
                    src="http://www.youtube.com/embed/M7lc1UVf-VE?autoplay=1&origin=http://example.com"
                    frameBorder="0"/>
        </div>
    )
}

export default PlayVideo;