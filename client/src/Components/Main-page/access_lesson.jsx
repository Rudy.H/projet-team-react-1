import {Link} from "react-router-dom";
import React, {useState, useEffect} from "react";
import axios from "axios";
import '../Main-page/access_lesson.css'

export default function AccessLesson() {

    const [state, setstate] = useState([]);

    useEffect(async () => {
        const result = await axios.get('http://159.65.28.230:3080/lesson/', {headers: {Authorization: 'Bearer ' + localStorage.getItem('access_token')}});
        setstate(await result.data);
    }, [])

    const post = async () => {
        await axios.post('http://159.65.28.230:3080/lesson', {title: "new lesson"} ,{headers: {Authorization: 'Bearer ' + localStorage.getItem('access_token')}});;
    }

    return (
        <>
            <div className="container-fluid">
            <button className="registerbutton" onClick={post}>Créer une leçon</button></div>
            <div className="containerback w-50">
                {state.map((elem, i) => {
                    return (<>
                        <Link to={'/admin/' + elem._id} className="lessonlist">
                            <a key={i}>
                                {elem.title}
                            </a>
                        </Link>
                        <hr/>
                    </>)
                })}
            </div>
        </>
    )
};

