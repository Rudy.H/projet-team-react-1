import {FormGroup} from "react-bootstrap";
import Form from "react-bootstrap/Form";
import HeaderForm from "./header";
import ButtonList from "./buttonlist";
import RegisterButton from "../Button/register-button";
import {useParams} from "react-router-dom";
import React, {useState, useEffect} from "react";
import axios from "axios";
import Button from "react-bootstrap/Button";

export default function AddLesson () {
    const [title, setTitle] = useState("");

    let id = useParams();

    useEffect(async () => {
        const result = await axios.get('http://159.65.28.230:3080/lesson/' + id.id, {headers: {Authorization: 'Bearer ' + localStorage.getItem('access_token')}});
        setTitle(await result.data);
    }, [])
    console.log(title);

    const update = async () => {
        const result = await axios.put('http://159.65.28.230:3080/lesson/' + id.id, {title: title} ,{headers: {Authorization: 'Bearer ' + localStorage.getItem('access_token')}});
        console.log(result);
    }

    return (
    <>
        <HeaderForm/>
        <div className="container-fluid w-50 mt-5 test2">
            <FormGroup className="lesson-name row py-4 px-4">
                <Form.Control className="formbg mr-4 form2" type="text" name="title"
                              placeholder={'Nom de la lesson ...'}
                              value={title.title} onChange={e => setTitle(e.target.value)}/>
                <Button className="registerbutton" block size="lg" type="submit"
                        onClick={update}>Enregistrer</Button>
            </FormGroup>
            <ButtonList lesson={title}/>
        </div>
    </>
)
}