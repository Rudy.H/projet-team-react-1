import React, {useState} from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import AuthLogin from "../../Services/auth.service";

export default function Login(props) {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const handleLogin = async (e) => {
        e.preventDefault();
        await AuthLogin(username, password);
         window.location.href="/admin";
    }

    return (
        <div className="bg">
            <div className="Login container-fluid">
                <h2 className="row justify-content-center logintitle title2">Editeur de Formation</h2>
                <div className="line w-50"/>
                <h3 className="row justify-content-center logintitle2 title2">Connexion</h3>

                <Form onSubmit={handleLogin}>
                    <Form.Group size="lg" controlId="username" className="row justify-content-center">
                        <Form.Control
                            className="formbg form2"
                            autoFocus
                            type="username"
                            value={username}
                            placeholder={'Login ...'}
                            onChange={e => setUsername(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group size="lg" controlId="password" className="row justify-content-center mb-5">
                        <Form.Control
                            className="formbg form2"
                            type="password"
                            value={password}
                            placeholder={'Mot de passe ...'}
                            onChange={e => setPassword(e.target.value)}
                        />
                    </Form.Group>

                    <div className="row justify-content-center">
                        <Button block size="lg" type="submit"
                                className="button1 my-5 border-0">
                            Valider
                        </Button>
                    </div>
                </Form>
            </div>
        </div>
    );
}
