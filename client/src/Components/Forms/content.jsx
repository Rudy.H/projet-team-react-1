import Form from "react-bootstrap/Form";
import {useDropzone} from 'react-dropzone';
import React, {useMemo, useState} from 'react';
import Button from "react-bootstrap/Button";
import {useParams} from "react-router-dom";
import axios from "axios";

export default function Content(props) {

    let id = useParams();
    const update = async () => {
        const result = await axios.put('http://159.65.28.230:3080/lesson/' + id.id, {Content: {
                title: title,
                content: contain,
                color: color
            },} ,{headers: {Authorization: 'Bearer ' + localStorage.getItem('access_token')}});
        console.log(result);
    }

    const baseStyle = {
        borderColor: '#D4D4D4',
        borderStyle: 'dashed',
        transition: 'border .25s ease-in-out',
        boxSizing: 'border-box',
        height: '200px',
    };
    const activeStyle = {
        borderColor: '#D4D4D4'
    };

    const acceptStyle = {
        borderColor: '#D4D4D4'
    };

    const rejectStyle = {
        borderColor: '#D4D4D4'
    };
    const {
        getRootProps,
        getInputProps,
        isDragActive,
        isDragAccept,
        isDragReject,
    } = useDropzone({accept: 'image/*'});

    const style = useMemo(() => ({
        ...baseStyle,
        ...(isDragActive ? activeStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
        ...(isDragReject ? rejectStyle : {}),
    }), [
        isDragActive,
        isDragReject,
        isDragAccept,
    ]);

    const [contain, setContain] = useState(props.content.content);
    const [title, setTitle] = useState(props.content.title);
    const [color, setColor] = useState(props.content.color);
    const [position, setPosition] = useState(props.content.checkbox);

    return (<>
            <div className="container-fluid">
                <div className="row justify-content-center">
                    <h2 className="title1 my-3">Contenu</h2>
                </div>
                <Form.Group className="row justify-content-center">
                    <div className="col-7 mr-2">
                        <div className="row size">
                            <Form.Control
                                className="formbg mb-1" type="text" name="title" placeholder={'Titre ...'} value={title}
                                onChange={(e) => setTitle(e.target.value)}/>
                            <textarea
                                className="formbg col" value={contain}
                                placeholder={'Contenu ...'}
                                onChange={(e) => setContain(e.target.value)}/>
                        </div>
                    </div>
                    <div
                        className="col size d-flex align-items-center justify-content-center dropzone-img"{...getRootProps({style})}>
                        <input {...getInputProps()} />
                        <p> + Ajout image </p>
                    </div>
                </Form.Group>
                <Form.Group className="row mt-3" controlId="formBasicCheckbox">
                    <div className="row">
                        <div htmlFor="head" className="mx-3 text1 mt-1">Couleur de fond
                            <input className="color-font" type="color" name="head" id="head"
                                   value={color}
                                   onChange={(event) => setColor(event.target.value)}/>
                        </div>

                    </div>
                    <div className="row my-2 offset-3">
                        <div className="text1 position-img"
                             onChange={(event) => console.log(event)}>Positionnement de l'image

                            <input type="checkbox" value="left" id="left"/>
                            <label htmlFor="left">Gauche</label>

                            <input type="checkbox" value="right" id="right"/>
                            <label htmlFor="right">Droite</label>
                        </div>

                    </div>
                </Form.Group>
                <div className="row my-5 justify-content-center">
                    <Button className="registerbutton" block size="lg" type="submit"
                            onClick={update}>Enregistrer</Button>
                </div>
            </div>
        </>
    );
}
