import Form from "react-bootstrap/Form"
import {useDropzone} from 'react-dropzone';
import React, {useMemo, useState} from 'react';
import Button from "react-bootstrap/Button";
import {useParams} from "react-router-dom";
import axios from "axios";

export default function VideoForm(props) {

    const [url, setUrl] = useState(props.content.youtube);
    const [title, setTitle] = useState(props.content.title);

    let id = useParams();
    const update = async () => {
        const result = await axios.put('http://159.65.28.230:3080/lesson/' + id.id, {Video: {
                title: title,
                youtube: url
            },},{headers: {Authorization: 'Bearer ' + localStorage.getItem('access_token')}});
        console.log(result);
    }

    const {
        getRootProps,
        getInputProps,
        isDragActive,
        isDragAccept,
        isDragReject,
    } = useDropzone({accept: 'video/*'});

    const style = useMemo(() => ({}), [
        isDragActive,
        isDragReject,
        isDragAccept,
    ]);

    return (
        <>
            <div className="container-fluid w-75">
                <h2 className='text-center title1 videotitle my-3'>Vidéo</h2>
                <div className="row justify-content-center">
                    <Form.Control className="formbg formvideo form3" type="Titre Vidéo" placeholder="Titre vidéo..."
                                  value={title}
                                  onChange={(event => setTitle(event.target.value))}/>
                </div>
                <div className="row justify-content-center">
                    <Form.Control className="formbg formvideo form3 my-3" type="URL"
                                  placeholder="+ Ajout Vidéo Youtube"
                                  value={url}
                                  onChange={((event) => {setUrl(event.target.value)})}/>
                </div>
                <div className="row justify-content-center mb-4">
                    <div
                        className="d-flex justify-content-center align-items-center dropbox col mr-3" {...getRootProps({style})} >
                        <input {...getInputProps()} />
                        <p className="dropboxtext"> + Ajout Vidéo MP4 </p>
                    </div>
                    <div
                        className="d-flex justify-content-center align-items-center dropbox col" {...getRootProps({style})}>
                        <input {...getInputProps()} />
                        <p className="dropboxtext"> + Ajout Vidéo Youtube </p>
                    </div>
                </div>
                <div className="row justify-content-center mb-4">
                    <Button className="registerbutton" type="submit"
                            onClick={update}>Enregistrer</Button>
                </div>
            </div>
        </>
    )
}
