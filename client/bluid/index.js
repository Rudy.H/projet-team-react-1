const express = require('express');
const app = express();
const path = require('path');
const server = require('http').createServer(app);
const port = 8080;
server.listen(port, () => {
    console.log('Server listening: http://localhost:%d', port);
});
app.use(express.static(path.join(__dirname, 'public')));