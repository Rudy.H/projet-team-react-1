const User = require('../../models/user')
const jwt = require('jsonwebtoken');

class AuthController {

    register(req, res){
        const account = new User(req.body);
        account.save().then(() => res.send({message: 'Nouveau compte enregistré !'})).catch(err => res.status(500).send({erreur: err}));
    }

    getUser(req, res) {
        res.send(req.user)
    }

    login(req, res) {
        User.findOne({username: req.body.username, password: req.body.password})
            .exec((err, user) => {
                if (!!err) {
                    res.status(500).send({message: "Une erreur est survenue lors de votre tentative de connexion !"});
                }
                if (!!user) {
                    res.send({
                        message: 'Authentification réussie',
                        token: jwt.sign(
                            {username: user.username,
                                password: user.password},
                            'ACscdFKqHYP4q9Ve', {
                            subject: user._id.toString()
                        }),
                        user
                    })
                }
            });
    }

    logout(req, res) {
        res.send('logout');
    }
}

module.exports = new AuthController();
