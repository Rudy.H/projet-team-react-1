const Lesson = require('../../models/Lesson');

class LessonController {

    index(req, res) {
        Lesson.find((err, lesson) => {
            res.send(lesson);
        });
    }


    async save(req, res) {
        const lesson = new Lesson({
            title: req.body.title,
            Content: {
                title: '',
                content: '',
                color: '',
                img: '',
                checkbox: true
            },
            Quiz: {
                title: '',
                question: '',
                answers: [{
                    value: false,
                    text: 'réponse 1'
                }]
            },
            Video: {
                title: '',
                youtube: '',
            },
        });
        await lesson.save();
        return res.status(201).send({message: "Lesson created successfully!"});
    }

   async getById(req, res) {
        const id = req.params.id;

        const MyLesson = await Lesson.findById(id);
        return res.status(200).send(MyLesson);
    }


    update(req, res) {

        const id = req.params.id;
        return Lesson.findByIdAndUpdate(id, req.body, {new: true}, (err, lesson) => {

            if (!!err) {
                res.status(404).send({message: "Lesson not found!"});
            }

            res.status(200).send({message: "Lesson updated successfully!", lesson});

        });

    }

    delete(req, res) {
        const id = req.params.id;
        Lesson.findByIdAndDelete(id, req.body, (err) => {
            if (!!err) {
                res.status(404).send({message: "Lesson not found!"});
            }
            res.status(200).send({message: "Lesson deleted successfully!"});
        });

    }
}

module.exports = new LessonController();

