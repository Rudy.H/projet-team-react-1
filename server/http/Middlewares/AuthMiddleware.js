const jwt = require('jsonwebtoken');
const User = require('../../models/user')

const AuthMiddleware = function(req, res, next) {

    const token = req.headers.authorization.split(' ')[1];

    jwt.verify(token, 'ACscdFKqHYP4q9Ve', (err, decoded) => {
        if(!!err) {
            res.status(401).send({message: 'Vous n\'êtes pas connecté !'});
        }
        User.findById(decoded.sub).exec((err, user) => {
            if(!!err) {
                res.status(401).send({message: 'Vous n\'êtes pas connecté !'});
            }

            console.log('user :', user);
            req.user = user;
            next();
        });
    });
};

module.exports = AuthMiddleware;
