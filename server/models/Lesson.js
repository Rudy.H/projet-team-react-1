const mongoose = require('mongoose');

const LessonSchema = new mongoose.Schema({
    title: String,
    Content: {
        title: String,
        content: String,
        color: String,
        img: String,
        checkbox: Boolean
    },
    Quiz: {
        title: String,
        question: String,
        answers: [{
            value: Boolean,
            text: String
        }]
    },
    Video: {
        title: String,
        youtube: String,
    },
    createdAt: {type: Date, default: new Date()},
    status: {type: Boolean, default: false}
});

module.exports = mongoose.model('Lesson', LessonSchema);
