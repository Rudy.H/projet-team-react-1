const express = require('express');
const cors = require('cors');

const AuthRouter = require('./router/AuthRouter');
const LessonRouter = require('./router/LessonRouter');

const server = express();

server.use(cors({
    origin: "*",
    optionsSuccessStatus: 200,
}));

server.use(express.json());
server.use(AuthRouter.prefix, AuthRouter.router);
server.use(LessonRouter.prefix, LessonRouter.router);

server.listen(3080, () => console.log('Server is running on http://localhost:8080'));

module.exports = server;
