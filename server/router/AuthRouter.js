const express = require('express');

const AuthController = require('../http/Controllers/AuthController')
const AuthMiddleware = require("../http/Middlewares/AuthMiddleware");
const router = express.Router();
const prefix = '/auth';

router.get('/user', AuthMiddleware, AuthController.getUser);
router.post('/login', AuthController.login);
router.post('/logout', AuthMiddleware, AuthController.logout);
router.post('/register', AuthController.register);

module.exports = {
    prefix,
    router
};
